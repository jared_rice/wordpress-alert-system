<?php
if( ! function_exists('si_acf_add_local_field_group_alerts') ) {

    function si_acf_add_local_field_group_alerts() {

        acf_add_local_field_group(array(
            'key' => 'group_sialert1',
            'title' => 'Alerts',
            'fields' => array(
                array(
                    'key' => 'field_5e7e027b1f0e9',
                    'label' => 'Alert Type',
                    'name' => 'si_alert_type',
                    'type' => 'radio',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'acfe_permissions' => '',
                    'choices' => array(
                        'emergency' => 'Emergency: ( Red with " ! " in a circle )',
                        'info' => 'Information: ( Blue with an " i " in a circle )',
                        'warning' => 'Warning: ( Yellow with an " ! " in a triangle )',
                    ),
                    'allow_null' => 0,
                    'other_choice' => 0,
                    'default_value' => '',
                    'layout' => 'vertical',
                    'return_format' => 'value',
                    'save_other_choice' => 0,
                ),
                array(
                    'key' => 'field_5e7e5685df7c4',
                    'label' => 'Does this alert have a link?',
                    'name' => 'si_alert_link_conditional',
                    'type' => 'radio',
                    'instructions' => 'You can make this alert clickable and give your audience more information by choosing "Link to this post" or "Custom Link"',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'acfe_permissions' => '',
                    'choices' => array(
                        'no-link' => 'No Link',
                        'article-link' => 'Link to this post',
                        'custom-link' => 'Custom Link',
                    ),
                    'allow_null' => 0,
                    'other_choice' => 0,
                    'default_value' => 'no-link',
                    'layout' => 'vertical',
                    'return_format' => 'value',
                    'save_other_choice' => 0,
                ),
                array(
                    'key' => 'field_5e7dffb9f13ef',
                    'label' => 'CustomLink',
                    'name' => 'si_alert_custom_link',
                    'type' => 'text',
                    'instructions' => 'To attach a link to this alert please place a link into the field below. You may enter an external URL or link to an existing link on the website.',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_5e7e5685df7c4',
                                'operator' => '==',
                                'value' => 'custom-link',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'acfe_permissions' => '',
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'si_alerts',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'acf_after_title',
            'style' => 'seamless',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            'acfe_display_title' => '',
            'acfe_autosync' => '',
            'acfe_permissions' => '',
            'acfe_form' => 0,
            'acfe_meta' => '',
            'acfe_note' => '',
        ));
    }
}

add_action('acf/init', 'si_acf_add_local_field_group_alerts');