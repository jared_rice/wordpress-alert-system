<?php 

if( ! function_exists('si_acf_add_acf_add_alerts_options_page') ) {

    function si_acf_add_acf_add_alerts_options_page() {

        acf_add_options_page(array(
            'page_title' => 'Alert Settings',
            'menu_title' => 'Settings',
            'menu_slug' => 'settings',
            'capability' => 'edit_posts',
            'position' => '',
            'parent_slug' => 'edit.php?post_type=si_alerts',
            'icon_url' => '',
            'redirect' => true,
            'post_id' => 'options',
            'autoload' => true,
            'update_button' => 'Update',
            'updated_message' => 'Options Updated',
        ));
    }
}

add_action('acf/init', 'si_acf_add_acf_add_alerts_options_page');


if( ! function_exists('si_acf_add_local_field_group_alert_settings') ) {

    function si_acf_add_local_field_group_alert_settings() {
            
       acf_add_local_field_group(array(
        'key' => 'group_5e8344326d037',
        'title' => 'Alert Settings',
        'fields' => array(
            array(
                'key' => 'field_5e8392654568a',
                'label' => 'Alert Options',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array(
                'key' => 'field_5e838e4c45681',
                'label' => 'Site Wrapper',
                'name' => 'si_alert_site_wrapper',
                'type' => 'text',
                'instructions' => 'Add your websites container class below if you don\'t want the alert inner text to be full width',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'default_value' => '',
                'placeholder' => 'container',
                'prepend' => '.',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5e8347271aad0',
                'label' => 'Alert Positioning',
                'name' => 'si_alert_positioning',
                'type' => 'radio',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'choices' => array(
                    'default' => 'Default',
                    'relative' => 'Relative',
                    'fixed' => 'Fixed',
                    'absolute' => 'Absolute',
                ),
                'allow_null' => 0,
                'other_choice' => 0,
                'default_value' => 'default',
                'layout' => 'vertical',
                'return_format' => 'value',
                'save_other_choice' => 0,
            ),
            array(
                'key' => 'field_5e838f1d45682',
                'label' => 'Alert Customization',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array(
                'key' => 'field_5e838f6145683',
                'label' => 'Information Alert Color',
                'name' => 'si_alert_information_alert_color',
                'type' => 'color_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'default_value' => '#46B5D2',
            ),
            array(
                'key' => 'field_5e838f9145684',
                'label' => 'Information Alert Color Hover',
                'name' => 'si_alert_information_alert_color_hover',
                'type' => 'color_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'default_value' => '#2D9CB9',
            ),
            array(
                'key' => 'field_5e838fac45685',
                'label' => 'Emergency Alert Color',
                'name' => 'si_alert_emergency_alert_color',
                'type' => 'color_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'default_value' => '#e53935',
            ),
            array(
                'key' => 'field_5e838fc345686',
                'label' => 'Emergency Alert Color Hover',
                'name' => 'si_alert_emergency_alert_color_hover',
                'type' => 'color_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'default_value' => '#CC201C',
            ),
            array(
                'key' => 'field_5e838fd345687',
                'label' => 'Warning Alert Color',
                'name' => 'si_alert_warning_alert_color',
                'type' => 'color_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'default_value' => '#f3bf12',
            ),
            array(
                'key' => 'field_5e83900a45688',
                'label' => 'Warning Alert Color Hover',
                'name' => 'si_alert_warning_alert_color_hover',
                'type' => 'color_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'default_value' => '#dbac11',
            ),
            array(
                'key' => 'field_5e83929a4568c',
                'label' => 'Advanced Options',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array(
                'key' => 'field_5e8344422c376',
                'label' => 'Custom CSS',
                'name' => 'si_alert_custom_css',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'acfe_permissions' => '',
                'default_value' => '#si-header-alerts {

    }',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 30,
                'new_lines' => '',
                'acfe_textarea_code' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'settings',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'seamless',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'acfe_display_title' => '',
        'acfe_autosync' => array(
            0 => 'php',
        ),
        'acfe_permissions' => '',
        'acfe_form' => 0,
        'acfe_meta' => '',
        'acfe_note' => '',
    ));

    }
}

add_action('acf/init', 'si_acf_add_local_field_group_alert_settings');