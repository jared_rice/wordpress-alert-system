<?php
function cptui_register_my_cpts_si_alerts() {
	/**
	 * Post Type: Alerts.
	 */
	$labels = [
		"name" => __( "Alerts", "understrap" ),
		"singular_name" => __( "Alert", "understrap" ),
	];
	$args = [
		"label" => __( "Alerts", "understrap" ),
		"labels" => $labels,
		"description" => "This Post Type adds the ability to create Alerts that appear at the top of every page on the website.",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "alerts", "with_front" => true ],
		"query_var" => true,
		"menu_position" => 50,
		"menu_icon" => "dashicons-warning",
		"supports" => [ "title", "page-attributes" ],
    ];
    register_post_type( "si_alerts", $args );
}
add_action( 'init', 'cptui_register_my_cpts_si_alerts', 10 );
