var debug = false;
var totalAlerts;
var indexAlert;

function loadAlertSlideshow() {
    if (jQuery('#alerts-slider').length) {
        jQuery('#alerts-slider').flexslider({
            animation: "slide",
            selector: '.alerts > .alert',
            directionNav: true,
            controlNav: false,
            pauseOnHover: true,
            slideshowSpeed: 5000,
            start: function (slider) {
                if (!Cookies.get('alertSeen') || debug == true) {
                    Cookies.set('alertSeen', true);
                }
                else {
                    jQuery('html').addClass('no-alert-transition');
                }
                jQuery('#si-header-alerts').addClass('loaded');
                jQuery('html').addClass('alert-active');
                setTimeout(function () { jQuery('html').removeClass('no-alert-transition'); }, 250);
                getCurrentAlert(slider);
            },
            after: function (slider) {
                getCurrentAlert(slider);
            }
        });
    }
}

function getCurrentAlert(slider) {
    var indexAlert = slider.currentSlide + 1;
    var totalAlerts = slider.pagingCount;
    if (totalAlerts > 1) {
        if (!jQuery('#si-header-alerts').hasClass('multiple')) {
            jQuery('#si-header-alerts').addClass('multiple');
        }
        jQuery('#flex-direction-nav').show();
        jQuery('#alerts-pager').html('<span class="first">' + indexAlert + '</span><span class="slash">/</span><span class="second">' + totalAlerts + '</span>');
        var alertType = jQuery('#si-header-alerts .alert.flex-active-slide').attr('data-alert-type');
        if (alertType == "emergency") {
            jQuery('#si-header-alerts').addClass('danger');
            jQuery('#si-header-alerts').removeClass('warning');
        }
        else if (alertType == "warning") {
            jQuery('#si-header-alerts').addClass('warning');
            jQuery('#si-header-alerts').removeClass('danger');
        }
        else {
            jQuery('#si-header-alerts').removeClass('danger');
            jQuery('#si-header-alerts').removeClass('warning');
        }
    }
    else {
        jQuery('#flex-direction-nav').hide();
    }
}

function getInitialAlertStatus() {
    var alertType = jQuery('#alerts-slider .alert:first').attr('data-alert-type');
    if (alertType == "emergency") {
        jQuery('#si-header-alerts').addClass('danger');
    } else if (alertType == "warning") {
        jQuery('#si-header-alerts').addClass('warning');
    }

}


jQuery(document).ready(function () {
    getInitialAlertStatus();
    loadAlertSlideshow();
});
