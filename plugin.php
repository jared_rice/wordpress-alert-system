<?php
/**
 * Plugin Name: SI Alert System
 * Plugin URI: https://bitbucket.org/jared_rice/wordpress-alert-system/
 * Description: Alert System Plugin that adds an alert banner to the header which can be customized
 * Version: 1.0.0
 * Author: Systems Insight
 * Author URI: https://systemsinsight.com
 *
 * @category Alert Systems
 * @author Systems Insight
 * @version 1.0
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Add Plugin Activation Class & Registers Required Plugins
 *
 * @since 1.0
 */
require_once plugin_dir_path( __FILE__ ) . "assets/plugin-activation/class-tgm-plugin-activation.php";

if ( ! function_exists( 'si_alert_system_register_required_plugins' ) ) {
    function si_alert_system_register_required_plugins() {

        $plugins = array(
            array(
                'name'      => 'Advanced Custom Fields',
                'slug'      => 'advanced-custom-fields',
                'required'  => true,
            )
        );
        tgmpa( $plugins ); 
    }
}

add_action( 'tgmpa_register', 'si_alert_system_register_required_plugins' );

/**
 * Enqueue Scripts needed for alert system
 *
 * @since 1.0
 */
if ( ! function_exists( 'si_alert_scripts' ) ) {
    function si_alert_scripts() {
        $url = untrailingslashit( plugin_dir_url( __FILE__ ) );
        //jQuery
        wp_enqueue_script( 'jquery' );
        //alerts
        wp_enqueue_script('si-alert-scripts', $url . '/src/js/si-alert.js');
        wp_enqueue_style('si-alert-styles', $url . '/src/css/si-alert.css');
        //flexslider
        wp_enqueue_script('flex-slider-scripts', $url . '/assets/flexslider/jquery.flexslider-min.js');
        wp_enqueue_style('flex-slider-styles', $url . '/assets/flexslider/flexslider.css');
        //cookie js
        wp_enqueue_script('cookie-js-scripts', $url . '/assets/cookiejs/js.cookie-2.2.1.min.js');
    }
}

add_action( 'wp_enqueue_scripts', 'si_alert_scripts' );

/**
 * Include all PHP files
 *
 * @since 1.0
 *
 * You can also do this instead of a foreach loop
 * - define( 'MY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
 * - include( MY_PLUGIN_PATH . 'src/php/si-alert.php');
 */
foreach ( glob( plugin_dir_path( __FILE__ ) . "src/php/*.php" ) as $file ) {
    include_once $file;
}


/**
 * Add Error log on activation for debug purposes
 *
 * @since 1.0
 */
add_action('activated_plugin','my_save_error');
function my_save_error()
{
    file_put_contents(dirname(__file__).'/error_activation.txt', ob_get_contents());
}
/**
 * Add Activation & Deactivation Functions
 * 
 * Activation And deactivation hooks should be put inside of a class
 * with static methods or WP will generate a unexpected output error
 *
 * @since 1.0
 */
class SI_Alerts_Plugin
{
    public static function si_flush_rewrites() {
        if ( ! get_option( 'si_flush_rewrite_rules_flag' ) ) {
            add_option( 'si_flush_rewrite_rules_flag', true );
        }
    }
    public static function si_unregister_post_type(){
        unregister_post_type( 'si_alerts' );
        flush_rewrite_rules();
    }
}
register_activation_hook( __FILE__, array( 'SI_Alerts_Plugin', 'si_flush_rewrites' ) );

register_deactivation_hook( __FILE__,  array( 'SI_Alerts_Plugin', 'si_unregister_post_type' ) );

/**
 * Flush rewrite rules if the previously added flag exists,
 * and then remove the flag.
 */
function si_flush_rewrite_rules_maybe() {
    if ( get_option( 'si_flush_rewrite_rules_flag' ) ) {
        flush_rewrite_rules();
        delete_option( 'si_flush_rewrite_rules_flag' );
    }
}
add_action( 'init', 'si_flush_rewrite_rules_maybe', 20 );